FROM python:alpine
COPY hello /
CMD ["/hello"]
COPY paulstretch_stereo.py /
COPY example.wav /
