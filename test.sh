#!/bin/bash

./hello | grep 'Hello World!' &> /dev/null
if [ $? == 0 ]; then
    echo "Output matches expectations."
    EXIT_CODE=0
else
    echo "Hello is broken"
    EXIT_CODE=1
fi

exit ${EXIT_CODE}

